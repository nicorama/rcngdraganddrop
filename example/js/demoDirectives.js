'use strict';

angular.module('rcMapDemo')
    .directive('demoBox', ['$log', '$compile', function ($log, $compile) {
        return{
            restrict: 'E',
            replace: true,
            template: '<div><p>I\'m a box</p></div>',
            scope: {
                isdroppable: '@',
                isdraggable: '@',
                color: '@'
            },
            compile: function (element, attrs) {
                if (attrs.isdroppable) {
                    element.removeAttr('isdroppable');
                    element.attr('rc-droppable', '');
                }
                if (attrs.isdraggable) {
                    element.removeAttr('isdraggable');
                    element.attr('rc-draggable', '');
                }
                var fn = $compile(element);
                return function (scope, iElement) {
                    iElement.addClass('box');
                    iElement.css({
                        position: 'absolute',
                        backgroundColor: scope.color,
                        width: attrs.width + 'px',
                        height: attrs.height + 'px',
                        top: attrs.top + 'px',
                        left: attrs.left + 'px'
                    });
                    fn(scope);
                }

            }
        }
    }
    ])
;
