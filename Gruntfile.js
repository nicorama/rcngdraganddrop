module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> (<%= pkg.homepage %>) */\n'
            },
            rcNgDradAndDrop: {
                files: {
                    './js/rcNgDragAndDrop.min.js': ['./js/rcNgDragAndDrop.js','./js/rcNgDraggable.js','./js/rcNgDroppable.js']
                }
            }
        },
        jshint: {
            options: {
                ignores: ['./js/*.min.js']
            },
            files: ['./js/*.js']
        },
        clean: {
            build: {
                src: ["dist"]
            }
        },
        connect: {
            server: {
                options: {
                    port: 9004,
                    open: {
                        target: 'http://localhost:9004/example/index.html'
                    }
                }
            }
        },
        watch: {
            js: {
                files: ['js/*.js','!js/*.min.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            html: {
                files: ['example/*.html'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('cleanbuild', ['clean']);
    grunt.registerTask('default', ['jshint','connect','watch']);
    grunt.registerTask('build', ['jshint','uglify']);
    grunt.registerTask('js', ['jshint']);

};