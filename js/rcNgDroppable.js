(function (window, angular) {
    'use strict';

    angular.module('rcNgDragAndDrop')
        .directive('rcNgDroppable', [ 'rcNgDragAndDropService', function (rcNgDragAndDropService) {
            return{
                restrict: 'A',
                scope: {
                    onDrop: '&'
                },
                link: function ($scope, $element) {
                    rcNgDragAndDropService.registerDroppable($element)
                        .then(null, null, function (ev) {

                            if (ev.type === 'enter') {
                                $element.addClass("dragOver");
                            } else if (ev.type === 'drop') {

                                ev.droppable = $element;
                                $scope.onDrop({ev: ev});
                                $element.removeClass("dragOver");

                            } else if (ev.type === 'exit') {
                                $element.removeClass("dragOver");
                            }
                        });
                }
            };
        }]);

})(window, window.angular);