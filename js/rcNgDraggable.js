(function (window, angular) {
    'use strict';

    angular.module('rcNgDragAndDrop')
        .directive('rcNgDraggable', ['DragDropHammer', 'hammerConfigDragDrop', 'rcNgDragAndDropService', function (Hammer, hammerConfigDragDrop, rcNgDragAndDropService) {
            return{
                restrict: 'A',
                scope: {
                    onDropped: '&',
                    onDragged: '&'
                },
                link: function ($scope, $element, $attrs) {

                    var posX = 0, posY = 0,
                        lastPosX = 0, lastPosY = 0,
                        domElement = $element[0];

                    var isNotDraggable = ($attrs.rcNgDraggable === 'false');
                    if (isNotDraggable)
                        return;

                    var hammerObj = new Hammer(domElement, hammerConfigDragDrop);

                    hammerObj.on('touch drag dragend', function (ev) {

                        ev.preventDefault();

                        if (ev.currentTarget !== ev.target)
                            return false;
                        var rect = $element.offset();
                        switch (ev.type) {
                            case 'touch':
                                lastPosX = rect.left;
                                lastPosY = rect.top;
                                $scope.onDragged({y: 'NaN', x: 'NaN'});
                                return false;

                            case 'drag':
                                this.style.zIndex = '1000';
                                domElement.style.position = 'absolute';
                                posX = ev.gesture.deltaX + lastPosX;
                                posY = ev.gesture.deltaY + lastPosY;
                                $(domElement).offset({left: posX, top: posY });
                                rcNgDragAndDropService.registerDragMouvement({y: posY, x: posX, el: this, $el: $(this), ev: ev });
                                $scope.onDragged({y: posY, x: posX});
                                return false;

                            case 'dragend':
                                this.style.zIndex = '';
                                lastPosX = posX;
                                lastPosY = posY;
                                rcNgDragAndDropService
                                    .registerDrop({y: posY, x: posX, el: this, $el: $(this), ev: ev  })
                                    .then(function (data) {
                                        $scope.onDropped({droppable: data.droppable, dropped: data.dropped});
                                    }, function ($elem) {
                                        $elem.css('position', '');
                                    });
                                return false;
                        }
                    });
                }
            };
        }
        ]);

})(window, window.angular);